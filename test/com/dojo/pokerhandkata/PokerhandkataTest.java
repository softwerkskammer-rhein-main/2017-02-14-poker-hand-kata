package com.dojo.pokerhandkata;

import org.junit.Test;

import java.util.Comparator;

import static org.junit.Assert.assertEquals;

/**
 * Created by Kazmi on 14.02.2017.
 */
public class PokerhandkataTest {

    @Test
    public void highCardWins()
    {
        PokerHand firsthand = new PokerHand("2H", "3D", "5S", "9C", "KD");
        PokerHand secondHand = new PokerHand("2C", "3H", "4S", "8C", "AH");

        assertEquals(-1, firsthand.compareTo(secondHand));
        assertEquals(1, secondHand.compareTo(firsthand));
    }



}
