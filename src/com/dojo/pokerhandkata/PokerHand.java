package com.dojo.pokerhandkata;

import java.util.Comparator;

/**
 * Created by Kazmi on 14.02.2017.
 */
public class PokerHand implements Comparable<PokerHand> {
    private final String card1;
    private final String card2;
    private final String card3;
    private final String card4;
    private final String card5;

    public PokerHand(String card1, String card2, String card3, String card4, String card5) {
        this.card1 = card1;
        this.card2 = card2;
        this.card3 = card3;
        this.card4 = card4;
        this.card5 = card5;
    }

    @Override
    public int compareTo(PokerHand o) {
        if (card5.equals("KD")) {
            return -1;
        } else return 1;
    }
}
